/*
    Array.prototype.flat(nivel_de_profundidad): un nuevo método que nos permite aplanar arreglos.
    Array.prototype.flatMap() lo mismo que flat con el beneficio de que primero manipular la data para luego poder aplanar.
*/

let array = [1,2,3, [1,2,3, [1,2,3]]];

console.log(array.flat()); // [1,2,3,1,2,3, [1,2,3]]
console.log(array.flat(2)); // [1,2,3,1,2,3,1,2,3]

let arrayb = [1,2,3,4,5];
const resultMultiplicado = arrayb.flatMap(value => [value, value * 2]);
console.log(resultMultiplicado) // [1,2,3,4,5,3,6,4,8,10]