// String.prototype.trimStart() | String.prototype.trimEnd() permite quitar los espacios al inicio o al final dependiendo de la funciona.

let hello = '        hello world';
console.log(hello);
console.log(hello.trimStart());

let hello = 'hello world        ';
console.log(hello);
console.log(hello.trimEnd());