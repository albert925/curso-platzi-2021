/*
    try/catch: ahora puedes utilizarlo sin necesidad de especificaro como catch(error) sino directamente usarlo en el scope del catch.
*/

try {
    
} catch {
    console.log(error);
}