/*
    Object.fromEntries() lo inverso a Object.entries(), es decir podemos convertir un objeto 
    en una matriz clave/valor con Object.entries(), 
    y hace lo inverso es decir de una matriz clave/valor a un objeto con Object.fromEntries().
*/

let entries = [['name', 'oscar'], ['age', 32]];

console.log(Object.fromEntries(entries));

let mysSimbol = `My simbol`;
let symbol = Symbol(mysSimbol);
console.log(symbol.description);