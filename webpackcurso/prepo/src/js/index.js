import React from 'react';
import { render } from 'react-dom';
import App from './components/App';

import '../css/estilos.css';

render(<App />, document.getElementById('container'));
