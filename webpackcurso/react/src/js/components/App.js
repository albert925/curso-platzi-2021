import React, { useState } from 'react';
import Loader from './Loader';
import data from './data.json';

console.log(data);
function App() {
    const [loaderList, setLoaderlist] = useState([]);
    const handleClick = () => setLoaderlist(data?.loaders || []);
    return(
        <div>
            <ul>
                {loaderList.map((item, index) => <Loader {...item} key={`item-${index.toString()}`} />)}
            </ul>
            <button type='button' onClick={handleClick}>Mostrar items</button>
        </div>
    );
}

export default App;