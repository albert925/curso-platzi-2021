const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: {
        // depedencias del producto
        modules: [
            'react',
            'react-dom'
        ]
    },
    mode: 'production',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].js', // los archivos de modulos que es exportan en js
        library: '[name]', // nopmbre global como libreria
    },
    plugins: [
        new webpack.DllPlugin({
            name: '[name]', // nombre del archivo dll. [name] nombre de los modules
            path: path.join(__dirname, '[name]-manifest.json'), // donde se exporta los dlls
        })
    ],
}