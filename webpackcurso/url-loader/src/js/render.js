function render(data) {
    const poke = document.createElement('img');
    poke.setAttribute('src', data?.sprites?.front_default);
    document.body.append(poke);
}

export default render;