const search = async (id) => {
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`);
    const poke = await response.json();
    return poke;
}

export default search;