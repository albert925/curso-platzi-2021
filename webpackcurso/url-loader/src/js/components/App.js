import React, { useState } from 'react';
import Loader from './Loader';
import data from './data.json';
import logo from '../../images/platzi.png';
import video from '../../video/que-es-core.mp4';

console.log(data);
function App() {
    const [loaderList, setLoaderlist] = useState([]);
    const handleClick = () => setLoaderlist(data?.loaders || []);
    return(
        <div>
            <h2>ttitle</h2>
            <p>
                <video src={video} width={360} controls poster={logo} />
            </p>
            <p>
                <img src={logo} alt='' width={40} />
            </p>
            <ul>
                {loaderList.map((item, index) => <Loader {...item} key={`item-${index.toString()}`} />)}
            </ul>
            <button type='button' onClick={handleClick}>Mostrar items</button>
        </div>
    );
}

export default App;