import React, { useState } from 'react';
import Loader from './Loader';
import data from './data.json';
import logo from '../../images/platzi.png';

import '../../sass/sass.scss';
import '../../less/less.less';
import '../../stylus/stylus.styl';

console.log(data);
function App() {
    const [loaderList, setLoaderlist] = useState([]);
    const handleClick = () => setLoaderlist(data?.loaders || []);
    return(
        <div>
            <p className="sass">Sass</p>
            <p className="less">Less</p>
            <p className="stylus">Stylus</p>
            <p className="post-css">Postcss</p>
            <h2>ttitle</h2>
            <p>
                <img src={logo} alt='' width={40} />
            </p>
            <ul>
                {loaderList.map((item, index) => <Loader {...item} key={`item-${index.toString()}`} />)}
            </ul>
            <button type='button' onClick={handleClick}>Mostrar items</button>
        </div>
    );
}

export default App;