const path = require('path');

module.exports = {
    entry: './index.js', // original
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }
}