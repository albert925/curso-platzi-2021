const path = require('path');

module.exports = {
    entry: {
        home: path.resolve(__dirname, 'src/js/index.js'),
    }, // original
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].js', // name es nombre de las keys del objecto
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader','css-loader']
            }
        ]
    }
}