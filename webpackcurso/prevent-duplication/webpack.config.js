const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    devServer:{
        hot: true,
        open: true, // abrir navegador
        port: 9000,
    },
    entry: {
        home: path.resolve(__dirname, 'src/js/index.js'),
        contact: path.resolve(__dirname, 'src/js/contact.js'),
    }, // original
    mode: 'production',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].js', // name es nombre de las keys del objecto
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    // para que no hagan conflicto se da la prioridad
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                        }
                    },
                    'postcss-loader',
                ]
            },
            {
                test: /\.(js|jsx)$/,
                use: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.jpg|png|gif|woff|eot|ttf|svg|mp4|webm|web$/,
                use: {
                    loader: 'url-loader',
                    options: {
                        limit: 90000, // convertir en base64 (mas pesado)
                    }
                },
            },
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'less-loader',
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.styl$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'stylus-loader',
                ]
            },
        ]
    },
    resolve: {
        extensions: [".js", ".jsx"]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            title: 'Plugins',
            template: path.resolve(__dirname, 'index.html')
        })
    ],
    optimization: {
        splitChunks: {
            chunks: 'all',// todas las importaciones
            minSize: 0, // tamaño minimo debe tener los chuks que debe ser considerado en los commons y metan los mas pesados
            name: 'commons', // como que se quiere que se llame el modulo que se va exportar donde se va agrupar el codigo que se repite en todas las paginas
        }
    }
}