const randomStrings = require('../index');

/*test('Probar la funcionalidad Random', () => {
    expect(typeof (randomStrings())).toBe('string');
});*/

describe('Probar funcionalidades de randomStrings', () => {
    test('Probar la funcionalidad Random', () => {
        expect(typeof (randomStrings())).toBe('string');
    });
    test('COmprobar que no existe una ciudad', () => {
        expect(randomStrings()).not.toMatch(/Cordoba/);
    });
});
