const helloWorld = () => {
  return new Promise((resolve, reject) => {
    (true)
      ? setTimeout(() => resolve('Hello world'), 3000) // resolve('Hello world')
      : reject(new Error('Test Error'))
  });
};

helloWorld()
  .then(res => console.log(res))
  .catch(err => console.log(err))
  .finally(() => console.log('finalizo'));