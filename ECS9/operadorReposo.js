const obj = {
  name: 'Oscar',
  age: 32,
  country: 'MX'
};

// all obtiene los valores que menos que asignaste antes
let { name, ...all } = obj;
console.log(name, all);

/**
 * Porpagation Properties
 */
const person = {
  name: 'Oscar',
  age: 32
};

const personInformation = {
  ...person,
  country: 'MX'
};
console.log(`personInformation: `, personInformation);