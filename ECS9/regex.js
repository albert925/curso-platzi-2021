// /([valores permitidos]{cantidad caracteres permiritdos})/
/*
  Ejemplo de un regex de una fecha 1999-02-02 [año-mes-dia]
  /([solo numeros]{4 numeros})-([solo numeros]{2 numeros})-([solo numeros]{2 numeros})
/*/

const regexData = /([0-9]{4})-([0-9]{2})-([0-9]{2})/;
const match = regexData.exec('2018-04-28');
const year = match[1];
const month = match[2];
const day = match[3];
console.log('Date -> ', year, month, day);
