/*
El proxy sirve para interceptar la lectura de propiedades de un objeto (los get, y set) 
entre muchas otras funciones. Así, antes de que la llamada llegue al objeto 
podemos manipularla con una lógica que nosotros definamos.
*/

const target = {
    red: 'rojo',
    gren: 'verde',
    blue: 'azul'
};

const handler = {
    get(obj, prop) {
        if (prop in object) {
           return obj[prop]
        }

        const suggestion = Object.keys(obj).find(keys => keys === 'gren');

        if (suggestion) {
            console.log(`${prop} no se encontró.`)
        }

        return obj[prop];
    }
}

const p = new Proxy(target, handler);
