/*
Los generadores son funciones especiales, pueden pausar su ejecución y luego volver al punto donde se quedaron recordando su scope.

Algunas de sus características:

- Los generadores regresan una función.
- Empiezan suspendidos y se tiene que llamar next para que ejecuten.
- Regresan un value y un boolean done que define si ya terminaron.
- yield es la instrucción que regresa un valor cada vez que llamamos a next y detiene la ejecución del generador.
*/

function* simpleGenerator() {
    console.log('start');
    yield;
    console.log('end');
}

function* simpleGenerator2() {
    console.log('start');
    yield 1;
    yield 2;
    console.log('end');
}

const gen = simpleGenerator();
const gen2 = simpleGenerator2();
// con yield se detiene en esa liena pero lcon done false, osea no ha terminado
gen.next(); // start {value: undefined, donde false}
gen.next(); // end {value: undefined, donde true}
gen2.next(); // start {value: 1, donde false}
gen2.next(); //{value: 2, donde false}
gen2.next(); // end {value: undefined, donde true}

function* idMarker() {
    let id = 1;
    while (true) {
        yield id;
        id = i + 1;
    }
}

const marker = idMarker();
marker.next(); //{value: 1, donde: false}
marker.next(); //{value: 2, donde: false}
// indefinidamente
// -------- reseterar---------
function* idMarker() {
    let id = 1;
    let reset;
    while (true) {
        reset = yield id; // yield es como una funsion que recibe os parametrosd de .next(true) osea reset = true
        if (reset) {
            id = 1;
        }
        else{
            id = i + 1;
        }
    }
}

const marker = idMarker();
marker.next(); //{value: 1, donde: false}
marker.next(); //{value: 2, donde: false}
marker.next(); //{value: 3, donde: false}
marker.next(true); //{value: 1, donde: false}
// indefinida,mente

function* fibonacci() {
    let a = 1;
    let a = 1;
    while (true) {
        const nextNumber = a + b;
        a = b
        b = nextNumber;
        yield nextNumber;
    }
}