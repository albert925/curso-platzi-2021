/*
En Javascript todo son objetos, no tenemos clases, no tenemos ese plano para crear objetos. Todos los objetos “heredan” 
de un prototipo que a su vez hereda de otro prototipo y así sucesivamente creando lo que se llama la prototype chain.
La keyword new crea un nuevo objeto que “hereda” todas las propiedades del prototype de otro objeto. 
No confundir prototype con proto que es sólo una propiedad en cada instancía que apunta al prototipo del que hereda.
*/

// Un objecto comun yt corriente
const zelda = {
    name: 'Zelda'
}

zelda.saludar = () => console.log(`hola ${this.name}`);

zelda.saludar();

const link = {
    name: 'Link'
}

link.saludar = () => console.log(`hola ${this.name}`);

link.saludar();

// Seamos un poco mas eficientes

function Hero(name) {
    const hero = {
        name,
    }

    hero.saludar = () => console.log(`hola ${this.name}`);

    return hero;
}

const zelda = Hero('Zelda');
zelda.saludar();

const link = Hero('Link');
link.saludar();

// aun podemos mejorar mas y evitar que crear la misma funcion cada vez
const heroMethods = {
    saludar: () => console.log(`hola ${this.name}`),
};

function Hero(name) {
    const hero = {
        name,
    }

    hero.saludar = heroMethods.saludar;

    return hero;
}

const zelda = Hero('Zelda');
zelda.saludar();

const link = Hero('Link');
link.saludar();

// object.create
//const nuevoObjecto = Object.create(ob)

const heroMethods = {
    saludar: () => console.log(`hola ${this.name}`),
};

function Hero(name) {
    const hero = Object.create(heroMethods)
    hero.name = name;
    return hero;
}

const zelda = Hero('Zelda');
zelda.saludar();

const link = Hero('Link');
link.saludar();

//los metodos de hero dentro de Hero

function Hero(name) {
    const hero = Object.create(Hero.prototype)
    hero.name = name;
    return hero;
}

Hero.prototype.saludar = console.log(`hola ${this.name}`);

const zelda = Hero('Zelda');
zelda.saludar();

const link = Hero('Link');
link.saludar();

// new es un atajo para llevar Hero.propotype al objecto -----------------------
function Hero(name) {
    this.name = name;
    // return this;
}

Hero.prototype.saludar = () => console.log(`New hola ${this.name}`);

const zelda = new Hero('Zelda');
zelda.saludar();

const link =new  Hero('Link');
link.saludar();

/* Herencia Prototipal
Por default los objetos en JavaScript tienen cómo prototipo a Object que es el punto de partida de todos los objetos, es el prototipo padre. 
Object es la raíz de todo, por lo tanto tiene un prototipo padre undefined.
Cuando se llama a una función o variable que no se encuentra en el mismo objeto que la llamó, 
se busca en toda la prototype chain hasta encontrarla o regresar undefined.
La función hasOwnProperty sirve para verificar si una propiedad es parte del objeto o si viene heredada desde su prototype chain.
*/

function Hero(name) {
    this.name = name;
    // return this;
}

Hero.prototype.saludar = () => console.log(`New hola ${this.name}`);

const zelda = new Hero('Zelda');

// propiedades de la instancia
console.log(zelda.name)
// propiedades de la clase
console.log(zelda.saludar);
// propiedades heredadas ej: toString
console.log(zelda.toString);
// hasOwnProperty (de donde sale toString o esto?)
console.log(`zelda.hasOwnProperty(name): ${zelda.hasOwnProperty('name')}`);
// ver lode __proto__
console.log(Object.getPrototypeOf(zelda));

// search prototypes
const protorypeOfZelda = Object.getPrototypeOf(zelda);

protorypeOfZelda.hasOwnProperty('toString');//false

const protorypeOfPrototypeOfZelda = Object.getPrototypeOf(Hero.prototype);

protorypeOfPrototypeOfZelda.hasOwnProperty('toString');// true;