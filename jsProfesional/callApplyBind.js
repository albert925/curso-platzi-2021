/*
Estas funciones nos sirven para establecer el valor de this, es decir cambiar el contexto que se va usar cuando la función sea llamada.

Las funciones call, apply y bind son parte del prototipo Function. Toda función usa este prototipo y por lo tanto tiene estas tres funciones.

functionName.call(). Ejecuta la función recibiendo como primer argumento el this y los siguientes son los argumentos que recibe la función que llamó a call.
functionName.apply(). Ejecuta la función recibiendo como primer argumento el this y como segundo un arreglo con los argumentos que recibe la función que llamó a apply.
functionName.bind(). Recibe como primer y único argumento el this. No ejecuta la función, sólo regresa otra función con el nuevo this integrado
*/
// Establece this usando call
function saludar() {
    console.log(`Hola soy ${this.name} ${this.apellido}`);
}

const ricard = {
    name: 'Ricard',
    apellido: 'López',
};

saludar.call(ricard);

function caminar(metro, direccion) {
    console.log(`${this.name} camina ${metro} hacia ${direccion}`);
}

caminar.call(ricard, 400, "norte");

// apply

caminar.apply(ricard, [300, 'sur']);

/*
    call - come
    apply - array
*/

// Bind
const daniel = { name: 'daniel', apellid: 'Sánchez' };
const danielSaluda = saludar.bind(daniel);
danielSaluda();

const danielCamina = caminar.bind(daniel);
danielCamina(1000, 'noreste');
// ---------- diferentes formas de pasar argumentos
const danielCamina = caminar.bind(daniel, 1000, 'noreste');
danielCamina()
// --------------
const danielCamina = caminar.bind(daniel, 2000);
danielCamina('noreste');


// NodeList
const buttons = document.getElementsByClassName('call-action');
Array.prototype.forEach.call(buttons, button => {
    button.onClick = ()  => alert('texto')
})