const data = {
    frontend: 'F',
    backend: 'B',
    design: 'D',
};

// como si combirtiera el objecto en arrays o arreglos
const entries = Object.entries(data);

console.log(entries);
// contar los elementos del objecto
console.log(entries.length);


const values = Object.values(data);
// retorna los valores
console.log(values);
console.log(values.length);

