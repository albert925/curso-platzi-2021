/*
  Es importante tener en cuenta la diferencia entre las promesas y async await. 
  Porque se encargan de lo mismo, la asíncronia, pero de distinta manera. 
  Por ello te dejo un recurso que te puede ayudar para entender esto. 
  Las promesas y async await, lógicamente, no son iguales
  https://platzi.com/tutoriales/1789-asincronismo-js/5063-las-promesas-y-async-await-logicamente-no-son-iguales-y-te-explico-el-porque/
*/

const helloWorld = () => {
  return new Promise((resolve, reject) => {
    (true)
      ? setTimeout(() => resolve('Hello world'), 3000)
      : reject(new Error('Test Error'))
  });
};

const helloAsync = async () => {
  const hello = await helloWorld();
  console.log(hello);
}

helloAsync();

const anotherFunction = async () => {
  try {
    const hello = await helloWorld();
    console.log(hello);
  } catch (error) {
    console.log(error);
  }
}

anotherFunction();
