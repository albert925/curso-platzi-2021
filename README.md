# curso-platzi-2021

Patrones de diseño
 
➡️Creacionales
Proveen diferentes mecanismos para crear objetos.

Abstract Factory
Builder
Factory Method
Prototype
Singleton
 
➡️Estructurales
Describen formas de componer objetos para formar nuevas estructuras flexibles y eficientes.
Adapter
Bridge
Composite
Decorator
Facade
Flyweight
Proxy
 
➡️Comportamiento
Gestionan algoritmos y responsabilidades entre objetos.
Chain of Responsibility
Command
Interpreter
Iterator
Mediator
Memento
Observer
State
Strategy
Template Method
Visitor

# Configurar webpack para trabajar con múltiples páginas en React
https://platzi.com/tutoriales/1620-webpack/7387-configurar-webpack-para-trabajar-con-multiples-paginas-en-react/

# webpack-cli
webpack-cli, la API que expone webpack en forma de CLI (Command Line Interface) que nos va a permitir interactuar y configurar Webpack desde la terminal

# Soporte imágenes, fuentes y videos
Para soportar la importación de archivos binarios en nuestro código Javascript cómo lo son: fuentes, imágenes y videos, podemos usar url-loader.

url-loader transforma archivos a un cadena de texto base64 para que carguen dentro de nuestros archivos Javascript y así ahorrarnos un request al servidor por cada archivo transformado.

Debemos tomar en cuenta que sólo nos conviene convertir archivos pequeños, ya que archivos muy grandes podrían hacer nuestro archivo bundle muy pesado. Es por esto que la opción limit del url-loader sirve para asignar el peso máximo que un archivo puede tener para ser transformado en base64.

No olvides instalar file-loader junto con url-loader ya que cuando se sobrepasa el limite establecido en la opción limit y el archivo no pueda ser transformado a base64, url-loader hará uso del file-loader para insertar un nombre y ruta de archivo en el lugar correspondiente.

# Evitar codigo duplicado
https://webpack.js.org/plugins/split-chunks-plugin/
https://github.com/LeonidasEsteban/webpack-4/commit/b897f995c7ae2ec97f1fc02e60306862ba194db0

Es útil dividir nuestro código en diversos archivos y a veces enteros proyectos, pero no queremos cargar nuestra aplicación de más multiplicando el peso de alguna dependencia al utilizarla en diferentes partes de la aplicación, para eso utilizamos el módulo de optimización con splitChunks en webpack.

es evitar la duplicacion de imports de la librerias al exportarlo

# Añadiendo un Dynamic Link Library
Mientras más librerías agregamos más lento se empiezan a volver nuestros builds, arruinando así la Developer Experience. Por suerte podemos crear una (o varias) Dynamic Link Library para acortar estos tiempos.

Una Dynamic Link Library (DLL) es un conjunto de librerías comunes que no cambian frecuentemente por lo que se hace un build por adelantado de las mismas para no re-empaquetar cada vez que hacemos build de nuestra aplicación.

Beneficiando tanto la Developer Experience como la User Experience ya que el caché del navegador va a mantener una copia que solo va a cambiar cuando nosotros agreguemos o quitemos alguna dependencia, ahorrando así valiosos requests al servidor.

Principio subyacente de la clase.

Mientras más dependencias más lento será tu build de production.
¿Qué acciones clave extraigo de esta clase ?

Para hacer el build más rápido vamos a aplicar la técnica de dynamic link library.

“Una Dynamic Link Library (DLL) es un conjunto de librerías comunes que no cambian frecuentemente por lo que se hace un build por adelantado de las mismas para no re-empaquetar cada vez que hacemos build de nuestra aplicación.”.

#Tutorial:
Cómo crear un Dynamic Link Library para acortar el tiempo de carga de librerias. (Estamos creando un webpack config super optimizado para produccion).

Creamos los siguientes archivos:
webpack.config.js (el webpack de toda la vida).
webpack.dll.config.js (el webpack para las librerias).
Colocamos las dependencias core que se repite su uso en nuestra app en el entry de nuestro webpack.dll.config.js. para guardarla en cache del navegador y precargarlas.
webpack.dll.config.js

entry: {
		modules: [
		"react",
		"react-dom"
		]
	},

Añadimos plugin de DLL en nuestro webpack.dll.config.js.
webpack.dll.config.js


plugins: [
		new webpack.DLLPlugin({
			name: "[name]",
			path: path.join(__dirname, "[name]-manifest.json")
		})
	]
Eliminamos lineas de código de developmente en nuestro webpack.dll.config.js.

Corres el dll wepack config y despues corres el otro webpack config con el script que creaste para npm run en tu package.json.

package.json

   
    "build:dll": "webpack --config ./dll/webpack.dll.config.js",
    "build:prevent-duplication-fast": "webpack --config ./dll/webpack.config.js"

cli


	$ npm run build:dll
	$ npm run build:prevent-duplication-fast

Añades el siguiente plugin en tu archivo webpack.config.js. El plugin optimizará tu CSS para producción, no está directamente relacionado con el DLL.

		new MiniCssExtractPlugin({
			filename: "css/[name].css",
			chunkFilename: "css/[id].css"
		})	,

Notas:

library es quien enlaza los modulos que creamos al codigo que escribimos de la app
Esta clase es dificil, practica, genera nuevas configuraciones.


# --------------
No me gusta mucho el recurso de hardcodear los archivos html de nuestra carpeta raíz. Así que me propuse dar como resultado esos archivos en nuestro bundle y que todo quede en automático. Lo logré hacer así:
.
1. Que como output se hagan dos archivos html dentro del dist: index.html y contact.html. Esto se hace declarando el HtmlWebpackPlugin más de una vez.
.
2. Que cada uno de esos archivos html mande a traer su propio js (index.js y contact.js). Esto se hace declarando en la propiedad ‘chunks’ (de cada uno de los HtmlWebpackPlugin) el chunk correspondiente.
.
3. Que los dos html resultantes manden a traer el modules.js de la carpeta dist en automático. Esto se puede hacer con el plugin AddAssetHtmlPlugin (https://github.com/SimenB/add-asset-html-webpack-plugin). Se tiene que instalar a través de npm (las instrucciones vienen en el enlace).
.
Ésta es la parte modificada del código (webpack.config.js):


.
Ésta es la parte modificada del código (webpack.config.js):

plugins: [
    new MiniCSSExtractPlugin({
      filename: 'css/[name].css',
      chunkFilename: 'css/[id].css'
    }),
    new HtmlWebpackPlugin({
      title: 'index',
      template: path.resolve(__dirname, 'index.html'),
      chunks: ['home']
    }),
    new HtmlWebpackPlugin({
      title: 'contact',
      filename: 'contact.html',
      template: path.resolve(__dirname, 'contact.html'),
      chunks: ['contact']
    }),
    new AddAssetHtmlPlugin({
      filepath: require.resolve('./dist/js/modules.js'),
    }),
    new webpack.DllReferencePlugin({
      manifest: require('./modules-manifest.json')
    })
  ]
.
Lo cual, da como resultado index.html dentro de dist:

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Home</title>
<link href="css/home.css" rel="stylesheet"></head>
<body>
  <div id="root"></div>
<script src="modules.js"></script><script src="js/home.js"></script></body>
</html>
.
Y contact.html dentro de dist:

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Contact</title>
<link href="css/contact.css" rel="stylesheet"></head>
<body>
  <div id="root"></div>
<script src="modules.js"></script><script src="js/contact.js"></script></body>
</html>
De esta forma, no es necesario estar hardcodeando los archivos html de la carpeta raíz, y tenemos lo que esperamos en la carpeta dist. Espero les ayude!

# Imports dinámicos

Resumen de la clase

1)) Instalar @babel/plugin-syntax-dynamic-import

npm install --save-dev --save-exact @babel/plugin-syntax-dynamic-import
2)) En .babelrc agregar el plugin:

        "plugins": [
            "@babel/plugin-syntax-dynamic-import"
        ]
3)) En el atributo output de webpack.config.js agregar lo siguiente:

publicPath: 'dist/',
chunkFilename: 'js/[id].[chunkhash].js'
Pd: Si el error “Module not found: Error: Can’t resolve '@babel/runtime/helpers/asyncToGenerator’” ocurre, instalar @babel/runtime, así:

        npm install --save-dev --save-exact @babel/runtime

        npm install -D @babel/plugin-syntax-dynamic-import

Aquí explica como configurar lo que es Splint Chunks y dynamic imports con react. Muy buen artículo que te muestra un caso real de como usarlos.
Quería aclarar que aun no aprendo react pero lo importante es entender como configura webpack.
https://itnext.io/react-router-and-webpack-v4-code-splitting-using-splitchunksplugin-f0a48f110312


<h3>Dynamic imports</h3>
Cuando el usuario haga una acción y en ese momento cargue un modulo a través de chunk especial , esto nos ayudará para mejorar nuestro tiempo de carga inicial y que este se muestra solo si el usuario lo solicita ya que pueda que ese modulo no todo mundo lo vea.

duplicamos la ultima carpeta dll y la nombramos dynamic-imports

Creamos nuestras tareas en package.json

        "build-dynamic-dll": "webpack --config ./dynamic-imports/webpackExterno.dll.config.js",
        "build-dynamic": "webpack --config ./dynamic-imports/webpackExterno.config.js"
        Para soporta esto dynamic import tenemos que instalar otro plugin a babel y lo hacemos con la versión del curso para evitar bug npm install @babel/plugin-syntax-dynamic-import@7.2.0 -D --save-exact

ahora agregamos este plugin en .babelrc

        {
            "plugins":[
                "@babel/plugin-syntax-dynamic-import",
                "@babel/plugin-transform-runtime"
            ],
            "presets": [
                "@babel/preset-env",
                "@babel/preset-react"
            ]
        }
modificamos nuestro webpack.config y agregamos este código a los output

        output:{
                /*A Donde vamos a buscar archivos nuevos o mis archivo que son mis assets aqui
                tambien se ponen los CDN optimizados   */ 
                publicPath: 'dist/',
                /*tambien vamos a personalizar los nombres de nuestro chunk en donde este tendra un 
                id y el chunkhash*/
                chunkFilename: 'js/[id].[chunkhash].js' 

            },
creamos un modulo que vamos a llamar de manera dinámica y lo creamos en la ruta src>js>component>alert.js

        /*Este le pasaremos un msj y mostrara una alert */
        function alerta(message){
            alert(message)
        }

        /*Este los vamos a exportar pero como un objeto*/
        export {
            alerta
        }
Dynamic no funcion en la configuración de webpack sino en el código para ello nos vamos a src>js>component>app.js y desde aquí llamaremos ese modulo de manera dinámica

        async function handleClick(){
                setLoaderList(data.loaders)
                /*Como estamos exportando un objeto pues la importacion se hacer de manera diferente
                no importandolo al inicio de nuestro codigo sino en donde lo vamos a llamar
                vamos a esperar que se termine la descarg y cuando lo haga vamos a guardar ese
                valor en una constante */
                const {alerta} = await import('./alert.js')
                //ahora los ejcutamos
                alerta('Este modulo ha cargado dinamicamente')
            }
primero ejecutamos npm run build-dynamic-dll este no se tiene que estar ejecutando constantemente cada que hagamos un cambio ya que son las dependencias core y las misma si no se han actualizado no es necesario volverlas a compilar y luego ejecutamos npm run build-dynamic

para probar nuestro código nos vamos a la raíz y abrimos index.html y probamos dando clic en el botón y veremos que nos salta la alerta, también podemos comprobar que si se carga al momento de darle clic abriendo el inspector de elementos > network y limpiamos, posteriormente le volvemos a dar clic al botón y vemos que en ese momento se carga un archivo 2.XXXXXX que es el [id.hash] que le configuramos como nombre.

# Git submodules

Me gustaría añadir una respuesta aquí que es realmente sólo un conglomerado de otras respuestas, pero creo que puede ser más completa.

Sabes que tienes un submómote Git cuando tienes estas dos cosas.

Su tiene una entrada como esta:.gitmodules

    [submodule "SubmoduleTestRepo"]
        path = SubmoduleTestRepo
        url = https://github.com/jzaccone/SubmoduleTestRepo.git


Tiene un objeto de submódulo (denominado SubmoduleTestRepo en este ejemplo) en el repositorio Git. GitHub los muestra como objetos de "submódulo". O hazlo desde una línea de comandos. Los objetos de submódulo Git son tipos especiales de objetos Git y mantienen la información SHA para una confirmación específica.git submodule status

Cada vez que haga un , rellenará su submódulo con contenido de la confirmación. Sabe dónde encontrar la confirmación debido a la información en el .git submodule update.gitmodules

Ahora, todo lo que hace es agregar una línea en su archivo. Así que siguiendo el mismo ejemplo, se vería así:-b.gitmodules

    [submodule "SubmoduleTestRepo"]
        path = SubmoduleTestRepo
        url = https://github.com/jzaccone/SubmoduleTestRepo.git
        branch = master


Nota: solo se admite el nombre de la sucursal en un archivo, pero SHA y TAG no son compatibles! (en lugar de eso, la confirmación de la rama de cada módulo se puede realizar un seguimiento y actualizar usando "", por ejemplo, como , y no es necesario cambiar el archivo cada vez) `.gitmodules git add .git add ./SubmoduleTestRepo.gitmodules`

El objeto de submódula sigue apuntando a una confirmación específica. Lo único que te compra la opción es la posibilidad de añadir una bandera a tu actualización según la respuesta de Vogella:`-b--remote`

    git submodule update --remote


En lugar de rellenar el contenido del submómogo a la confirmación señalada por el submódulo, reemplaza esa confirmación por la confirmación más reciente en la rama maestra, THEN rellena el submótido con esa confirmación. Esto se puede hacer en dos pasos por djacobs7 respuesta. Puesto que ahora ha actualizado la confirmación que apunta el objeto de submódulo, debe confirmar el objeto de submódulo modificado en el repositorio de Git.

`git submodule add -b` no es una manera mágica de mantener todo al día con una rama. Simplemente agrega información sobre una rama en el archivo y le da la opción de actualizar el objeto de submódulo a la confirmación más reciente de una rama especificada antes de rellenarlo. `.gitmodules`

FUENTE: https://stackoverflow.com/questions/1777854/how-can-i-specify-a-branch-tag-when-adding-a-git-submodule

# Create-react-app

Create-react-app es una aplicación moderna que se usa desde una línea de comando. Antes de ella se configuraba todo el entorno manualmente lo cual tomaba mucho tiempo.

Pasos para obtenerlo:
Se debe instalar desde la línea de comando usando
npm install -g create-react-app

Una vez instalado se crea la carpeta del proyecto con
create-react-app -nombre del proyecto-

En este punto se estará instalando React y otras herramientas, también se configurará el entorno usando Webpack.

Una vez se instala todo entra a la carpeta src donde estará todo el código fuente de la aplicación, siendo el más importante index.js que es el punto de entrada a la aplicación.

Finalmente para correr la aplicación se usa el comando
npm run start

Otras herramientas:

Babel: Traduce Javascript moderno (JSX) a un Javascript que todos los navegadores puedan interpretar.
Eslint: Lee el código y avisa de errores.

# Preparación del entorno con Jest
Escribimos en la terminal el siguiente comando:

npm install -D jest
  -D es lo mismo que --save-dev.

Ambos flags guardan la dependencia como una de desarrollo.

Trabajando con los basicos de Jest
Vamos a estar trabajando con la función “test”, esta recibe dos parametros:

Un string que describe lo que va a pasar.
Función anonima en la cual viene lo que se va a probar.
La función expect
Contiene dos parametros:

Valor de entrada.
Con lo que el primer parametro va a ser comparado.
const text = "I´m Ironman";

  test("It should has the word Ironman", () => {
    expect(text).toMatch(/Ironman/);
  });

Código listo para hacer test

Ahora para hacer test de nuestro código tenemos que correr el comando test, para eso se tiene que armar en nuestro package.json

  "scripts": {
    "test": "jest"
  },

Output

  PASS src/**test**/global.test.js
  ✓ It should has the word Ironman (4ms)

  Test Suites: 1 passed, 1 total
  Tests: 1 passed, 1 total
  Snapshots: 0 total
  Time: 1.663s
  Ran all test suites.

probar solo la prueba de un archivo

No es necesario instalar Jest de manera global para usar el comando de esa forma.
Hay dos opciones, una sería crear el comando en el package.json y correr el script correspondiente y la otra sería usando el prefijo npx así:

  npx jest src/__test__/index.test.js

`npx jest --watch` para dejar escuchando y tomar siempre cambios
`npx jest --coverage` generar un reporte primilinar y genera un reporte html para verlo en el navegador que crea un carpeta `/coverage/Icov-report`

# Webpack 5 ----------------------------------------------------------------------

# Configuración de webpack.config.js

Si queremos añadir el autocompletado para nuestro archivo de webpack, podemos añadir el siguiente código antes del module.exports

    /** @type {import('webpack').Configuration} */

Configuración de webpack.config.js
<h4>Apuntes</h4>
El archivo de configuración nos va ayudar a poder establecer la configuración y elementos que vamos a utilizar
Para poder crear el archivo de configuración en la raíz del proyecto creamos un archivo llamado webpack.config.js
En el mismo debemos decir
El punto de entrada
Hacia a donde a enviar la configuración de nuestro proyecto
Las extensiones que vamos usar

# Babel Loader para javascript
<h4>Apuntes</h4>
Babel te permite hacer que tu código JavaScript sea compatible con todos los navegadores
Debes agregar a tu proyecto las siguientes dependencias
NPM

    npm install -D babel-loader @babel/core @babel/preset-env @babel/plugin-transform-runtime

Yarn

    yarn add -D babel-loader @babel/core @babel/preset-env @babel/plugin-transform-runtime

babel-loader nos permite usar babel con webpack
- @babel/core es babel en general
- @babel/preset-env trae y te permite usar las ultimas características de JavaScript
- @babel/plugin-transform-runtime te permite trabajar con todo el tema de asincronismo como ser async y await
- Debes crear el archivo de configuración de babel el cual tiene como nombre .babelrc


    {
      "presets": [
        "@babel/preset-env"
      ],
      "plugins": [
        "@babel/plugin-transform-runtime"
      ]
    }


Para comenzar a utilizar webpack debemos agregar la siguiente configuración en webpack.config.js


    {
    ...,
    module: {
        rules: [
          {
            // Test declara que extensión de archivos aplicara el loader
            test: /\.js$/,
            // Use es un arreglo u objeto donde dices que loader aplicaras
            use: {
              loader: "babel-loader"
            },
            // Exclude permite omitir archivos o carpetas especificas
            exclude: /node_modules/
          }
        ]
      }
    }

RESUMEN: Babel te ayuda a transpilar el código JavaScript, a un resultado el cual todos los navegadores lo puedan entender y ejecutar. Trae “extensiones” o plugins las cuales nos permiten tener características más allá del JavaScript común

# HtmlWebpackPlugin

Es un plugin para inyectar javascript, css, favicons, y nos facilita la tarea de enlazar los bundles a nuestro template HTML.

Instalación
npm

    npm i html-webpack-plugin -D
    
yarn

    yarn add html-webpack-plugin -D

# Loaders para CSS y preprocesadores de CSS

<h4>Ideas/conceptos claves</h4>
Un preprocesador CSS es un programa que te permite generar CSS a partir de la syntax única del preprocesador. Existen varios preprocesadores CSS de los cuales escoger, sin embargo, la mayoría de preprocesadores CSS añadirán algunas características que no existen en CSS puro, como variable, mixins, selectores anidados, entre otros. Estas características hacen la estructura de CSS más legible y fácil de mantener.

post procesadores son herramientas que procesan el CSS y lo transforman en una nueva hoja de CSS que le permiten optimizar y automatizar los estilos para los navegadores actuales.

<h4>Apuntes</h4>
Para dar soporte a CSS en webpack debes instalar los siguientes paquetes
Con npm

    npm i mini-css-extract-plugin css-loader -D

Con yarn

    yarn add mini-css-extract-plugin css-loader -D

css-loader ⇒ Loader para reconocer CSS
mini-css-extract-plugin ⇒ Extrae el CSS en archivos
Para comenzar debemos agregar las configuraciones de webpack
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

    module.exports = {
      ...,
      module: {
        rules: [
          {
            test: /\.(css|styl)$/i,
            use: [
              MiniCssExtractPlugin.loader,
              "css-loader",
            ]
          }
        ]
      },
      plugins: [
        ...
        new MiniCssExtractPlugin(),
      ]
    } 


Si deseamos posteriormente podemos agregar herramientas poderosas de CSS como ser:
pre procesadores
Sass
Less
Stylus
post procesadores
Post CSS
RESUMEN: Puedes dar soporte a CSS en webpack mediante loaders y plugins, además que puedes dar superpoderes al mismo con las nuevas herramientas conocidas como pre procesadores y post procesadores

# Copia de archivos con Webpack 
NPM

    npm i copy-webpack-plugin -D

YARN

    yarn add copy-webpack-plugin -D 

# Loaders de imágenes

Este loader nos permite importar de forma dinámica en nuestros archivos JavaScript imágenes, el loader le genera un hash unico para cada imagen. Algo parecido sucede con ReactJS al importar imágenes
Configuración

Al final de las rules

    {
      test: /\.png/,
      type: 'asset/resource'
    }

Correcto. Además en Webpack 5 el módulo se encuentra incorporado. Donde antes, teníamos que incorporar los loaders url-loader y file-loaders a la descripción del módulo

Para agregar más tipos:

      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
      },

<h4>Apuntes</h4>
Puedes usar una forma de importar las imágenes haciendo un import de las mismas y generando una variable
No es necesario instalar ninguna dependencia, webpack ya lo tiene incluido debemos agregar la siguiente configuración


# Loaders de fuentes

En las options:

    options: {
            limit: 10000, // O LE PASAMOS UN BOOLEANOS TRUE O FALSE
            // Habilita o deshabilita la transformación de archivos en base64.
          mimetype: 'aplication/font-woff',
          // Especifica el tipo MIME con el que se alineará el archivo. 
          // Los MIME Types (Multipurpose Internet Mail Extensions)
          // son la manera standard de mandar contenido a través de la red.
          name: "[name].[ext]",
          // EL NOMBRE INICIAL DEL ARCHIVO + SU EXTENSIÓN
          // PUEDES AGREGARLE [name]hola.[ext] y el output del archivo seria 
          // ubuntu-regularhola.woff
          outputPath: './assets/fonts/', 
          // EL DIRECTORIO DE SALIDA (SIN COMPLICACIONES)
          publicPath: './assets/fonts/',
          // EL DIRECTORIO PUBLICO (SIN COMPLICACIONES)
          esModule: false 
        // AVISAR EXPLICITAMENTE SI ES UN MODULO
    }


Para las imagenes

    assetModuleFilename: 'assets/images/[hash][ext]'

Esta instrucción hace que webpack le agregue un hash ( un hash es una serie de caracteres aleatorios) y su extencion por medio de esas variables en el string

Te ayudo a entender algunas cosas. 😃
Rules
Webpack es muy personalizable, desde el module, la rule, los plugins por lo que se podrán observar muchas configuraciones.
Recuerda que url-loader ya está integrada como módulo en Webpack 5 por lo que no es necesario instalarla, a menos que desees configurar ciertos parámetros como lo son limit para especificar el tamaño máximo de tus archivos en bytes donde si el archivo es igual o más grande utiliza el file-loader para que procese su rendimiento, esModule es para permitir que file-loader genere modulos JS que usan la sintaxis ES. Documentatción de url-loader.

url-loader, incorporado en Webpack 5 permite configurar algunos parámetros genéricos.

Rule.mimetype - nos permite determinar el tipo de archivo que será enlazado o cargado.

    module.exports = {
      // ...
      module: {
        rules: [
          {
            mimetype: 'application/json',
            type: 'json',
          },
        ],
      },
    };

Notarás que será utilizado para JS en la carga de módulos que tendrán, o pueden tener, contendio estático.

Rule.generator.filename- Donde espeficamos el nombre de la salida para con la regla donde tu mencionas “EL NOMBRE DEL ARCHIVO FINAL” el cual no esta del todo mal, pero complementaria que más que un nombre, es una regla que acompañará los archivos mapeados con asset/resource
Al final, te presento el webpack.config.js que estoy maquetando:

    ...
            output: {
                path: resolve(__dirname, 'dist'),
                filename: 'bondle.js',
                assetModuleFilename: 'assets/images/[hash][ext][query]',
            },
    ...
            module: {
                rules: [
                    {
                        test: /\.(png|svg|jpg|jpeg|gif)$/i,
                        type: 'asset/resource',
                    },
                    {
                        test: /\.(woff|woff2|eot|ttf|otf)$/i,
                        type: 'asset/resource',
                        generator: {
                            filename: 'assets/fonts/[hash][ext]',
                        },
                    },
    ...


Configuraciones más, configuraciones menos, te adjunté los enlaces para que complementes tu documentación y puedas elaborar más tipos de archvios de configuración de webpack


link descarga de fuentes
http://google-webfonts-helper.herokuapp.com/fonts/ubuntu?subsets=latin

<h4>Apuntes</h4>
Cuando utilizamos fuentes externas una buena práctica es descargarlas a nuestro proyecto
Debido a que no hara un llamado a otros sitios
Por ello es importante usarlo dentro de webpack
Para esta tarea instalaras y usaras “file-loader” y “url-loader”
instalación con NPM

    npm install url-loader file-loader -D

instalación con YARN

    yarn add url-loader file-loader -D

# Optimización: hashes, compresión y minificación de archivos

<h4>Apuntes</h4>
Unos de las razones por que utilizamos webpack es porque nos permite optimizar y comprimir nuestro proyecto

Debes utilizar los siguientes paquetes
css-minimizer-webpack-plugin ⇒ Nos ayuda a comprimir nuestros archivos finales CSS
terser-webpack-plugin ⇒ Permite minificar de una mejor forma

Instalación
NPM

    npm i css-minimizer-webpack-plugin terser-webpack-plugin -D


YARN

    yarn add css-minimizer-webpack-plugin terser-webpack-plugin -D


Una vez instalado el plugin debemos agregar la siguiente configuración

    ...
    const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
    const TerserPlugin = require('terser-webpack-plugin');

    module.exports = {
      ...
      optimization: {
        minimize: true,
        minimizer: [
          new CssMinimizerPlugin(),
          new TerserPlugin()
        ]
      }
    }

Cuando nombremos en la configuración de webpack es importante usar [contenthash] para evitar problemas con la cache

# Webpoack Alias

<h4>Apuntes</h4>

Alias ⇒ nos permiten otorgar nombres paths específicos evitando los paths largos
Para crear un alias debes agregar la siguiente configuración a webpack

    module.exports = {
      ...
      resolve: {
        ...
        alias: {
          '@nombreDeAlias': path.resolve(__dirname, 'src/<directorio>'),
        },
      }
    }

Puedes usarlo en los imports de la siguiente manera

    import modulo from "@ejemplo/archivo.js";

No olvides comentar que alias forma parte del objeto resolve(https://webpack.js.org/configuration/resolve/#resolve) el cual nos permite configurar la forma en que webpack resolverá los módulos incorporados.
En nuestro camino, tenemos dos:

resolve.alias - para crear atajos que optimizan el tiempo de búsqueda e incorporación de módulos (commonJS o ES6)
resolve.extensions - para darle prioridad en resolución para con las extensiones donde si hay archivos nombrados igualmente, pero con diferentes extensiones, webpack resolverá conforme están listados.

# Variables de entorno
<h4>Apuntes</h4>
Es importante considerar las variables de entorno va a ser un espacio seguro donde podemos guardar datos sensibles
Por ejemplo, subir llaves al repositorio no es buena idea cuando tienes un proyecto open source
Para instalar debemos correr el comando

NPM

    npm install -D dotenv-webpack

YARN

    yarn add -D dotenv-webpack

Posteriormente debemos crear un archivo .env donde estarán la clave para acceder a la misma y el valor que contendrán

# Ejemplo

API=https://randomuser.me/api/
Es buena idea tener un archivo de ejemplo donde, el mismo si se pueda subir al repositorio como muestra de que campos van a ir
Una vez creado el archivo .env debemos agregar la siguiente configuración en webpack.config.js

    ...
    const Dotenv = require('dotenv-webpack');
    module.exports = {
      ...
      plugins: [
        new Dotenv()
      ],
    }


dotenv-webpack ⇒ Leera el archivo .env por defecto y lo agregar a nuestro proyecto
Para usarlas debes hacer lo siguiente
const nombre = process.env.NOMBRE_VARIABLE;
Toda la configuración se podrá acceder desde process.env

# Webpack en modo desarrollo

# Webpack en modo producción

<h4>Apuntes</h4>
Actualmente tenemos el problema de tener varios archivos repetidos los cuales se fueron acumulando por compilaciones anteriores
Para ello puedes limpiar la carpeta cada vez que hacemos un build, usando clean-webpack-plugin
Cabe recalcar que esta característica es mucho más util para la configuración de producción
Para instalarlo debes correr el siguiente comando:

NPM

    npm install -D clean-webpack-plugin

YARN

    yarn add -D clean-webpack-plugin

Para agregarlo a nuestra configuración de webpack agregamos los siguientes cambios a webpack.config.js

    ...
    const { CleanWebpackPlugin } = require('clean-webpack-plugin');
    module.exports = {
      ...
      plugins: [
        ...
        new CleanWebpackPlugin()
      ]
    }

# Webpack Watch

<h4>Apuntes</h4>

El modo watch hace que nuestro proyecto se compile de forma automática
Es decir que está atento a cambios
Para habilitarlo debemos agregar lo siguiente en la configuración de webpack


    module.exports = {
      ...
      watch: true
    }

Cada vez que haya un cambio hara un build automático
Otra manera es mandar la opción mediante parámetros de consola en package.json

    {
      "scripts": {
        "dev:watch": "webpack --config webpack.config.dev.js --watch"
      }
    }

Vale la pena recordar que si aplicamos en modo producción se tomara más tiempo porque se optimizaran los recursos
Por ello en modo desarrollo se salta ese paso y es más rápido la compilación

# Webpack Dev Server
<h4>Ideas/conceptos claves</h4>
HTML5 History API permite la manipulación de session history del navegador, es decir las páginas visitadas en el tab o el frame en la cual la página está cargada.

<h4>Recursos</h4>
How To Optimize Your Site With GZIP Compression

<h4>Apuntes</h4>
Cuando trabajamos con webpack deseamos ver los cambios en tiempo real en un navegador
Para tener esta característica esta webpack-dev-server
Para ello debemos instalarlo

NPM

    npm install -D webpack-dev-server

Yarn

    yarn add -D webpack-dev-server

Posteriormente debemos agregar la siguiente configuración en webpack.config.dev.js
Lo hacemos en la configuración de desarrollo debido a que esta característica solo nos ayudara a ver cambios al momento de desarrollar la aplicación


    module.exports = {
      ...
      devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        historyApiFallback: true,
        port: 3000,
      }
    }


En la configuración podemos observar lass siguientes propiedades
contentBase ⇒ Le dice al servidor donde tiene que servir el contenido, solo es necesario si quieres servir archivos estáticos
compress ⇒ Habilita la compresión gzip
historyApiFallback ⇒ cuando estas usando HTML5 History API la página index.html sera mostrada en vez de una respuesta 404
Port ⇒ es el puerto donde vamos a realizar las peticiones
Para comenzar a utilizarlo debes agregar el siguiente script a package.json

    {
      ...
      "scripts": {
      ...
      "start": "webpack serve --config webpack.config.dev.js"
      }
    }

# Webpack Bundle Analyzer

<h4>Apuntes</h4>
Cuando tenemos un proyecto es buena idea poder revisar su impacto en tamaño por ese motivo webpack nos ofrece un paquete para poder verificar y analizar el tamaño del bundle final
Para instalar corremos el comando
NPM

    npm install -D webpack-bundle-analyzer

YARN

    yarn add -D webpack-bundle-analyzer


Si deseamos hacer un análisis debemos correr los siguientes comandos

npx webpack --profile --json > stats.json
npx webpack-bundle-analyzer stats.json

## Nota para la siguiente clase
Actualmente si corremos el modo dev no funcionara, en el require de webpack-bundle-analyzer Oscar agrega lo siguiente

    const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

Es raro que no tire del modulo por default, supongo que esto también valdría

    const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

https://www.npmjs.com/package/webpack-bundle-analyzer

You can analyze an existing bundle if you have a webpack stats JSON file.

You can generate it using BundleAnalyzerPlugin with generateStatsFile option set to true or with this simple command:

    webpack --profile --json > stats.json

If you're on Windows and using PowerShell, you can generate the stats file with this command to avoid BOM issues:

    webpack --profile --json | Out-file 'stats.json' -Encoding OEM

Then you can run the CLI tool.

    webpack-bundle-analyzer bundle/output/path/stats.json

Options (for CLI)

    webpack-bundle-analyzer <bundleStatsFile> [bundleDir] [options]

# Webpack DevTools

<h4>Ideas/conceptos claves</h4>

source map es un mapeo que se realiza entre el código original y el código transformado, tanto para archivos JavaScript como para archivos CSS. De esta forma podremos debuggear tranquilamente nuestro código.

<h4>Apuntes</h4>
Con las devtools de webpack te permite crear un mapa de tu proyecto y con el podemos
Leer a detalle
Analizar particularidades de lo que está compilando nuestro proyecto
Para comenzar debemos ir a webpack.config.js y agregar la propiedad devtool: "source-map"
Esta opción genera un source map el cual posteriormente chrome lo lee y te permite depurar de una mejor forma